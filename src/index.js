import Post from './Post'
import "./scss/style.scss";

const saveUserInfo = () => {
    const name = document.querySelector('#name').value;
    const age = document.querySelector('#age').value;
    const sex = document.querySelector('#sex').value;

    let post = new Post(name,age,sex);
    post.setUserInfo();
};

document.querySelector('.save_info').addEventListener('click',saveUserInfo);